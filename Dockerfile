FROM ubuntu:23.10 as compiler
RUN apt-get update && apt-get install -y build-essential git
COPY hello.c /
RUN make hello
FROM ubuntu:23.10 as test
COPY --chmod=700 test.sh .
COPY --from=compiler hello .
ENTRYPOINT ./test.sh ynov oal
FROM ubuntu:23.10 as prod
LABEL MAINTAINER ynov
LABEL SERVICE hello
WORKDIR /home/ynov
COPY --from=compiler hello .
ENTRYPOINT ["/home/ynov/hello"]