#Hello.c

#include <stdio.h>

int main(int argc, char *argv[]) {
    // Si aucun argument n'est fourni on afficher Hello World!
    if (argc == 1) {
        printf("Hello !");
    } else {
        // Affiche le message Hello concatené avec chaque argument.
        for (int i = 1; i < argc; i++) {
            printf("Hello %s!\n", argv[i]);
        }
    }
    return 0;
}
